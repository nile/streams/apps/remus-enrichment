# Remus Enrichment

## Overview

### Owners & Administrators

| Title       | Details                                                                                |
|-------------|----------------------------------------------------------------------------------------|
| **E-group** | [remus-super-admin](https://groups-portal.web.cern.ch/group/remus-super-admin/details) |
| **People**  | [Adrien Robert Ledeul](https://phonebook.cern.ch/search?q=Adrien-Robert-Ledeul)        |

### Kafka Topics

| Environment    | Topic Name                                                                                                        |
|----------------|-------------------------------------------------------------------------------------------------------------------|
| **Production** | [remus-meas](https://nile-kafka-ui.app.cern.ch/ui/clusters/gptn3/all-topics/remus-meas)                           |
| **Production** | [remus-meas-enriched](https://nile-kafka-ui.app.cern.ch/ui/clusters/gptn3/all-topics/remus-meas-enriched)         |
| **Production** | [remustest-meas](https://nile-kafka-ui.app.cern.ch/ui/clusters/gptn3/all-topics/remustest-meas)                   |
| **Production** | [remustest-meas-enriched](https://nile-kafka-ui.app.cern.ch/ui/clusters/gptn3/all-topics/remustest-meas-enriched) |

### Configuration

| Title                        | Details                                                                                                    |
|------------------------------|------------------------------------------------------------------------------------------------------------|
| **Configuration Repository** | [app-configs/remus-enrichment](https://gitlab.cern.ch/nile/streams/app-configs/remus-enrichment)           |
| **Configuration Repository** | [app-configs/remus-test-enrichment](https://gitlab.cern.ch/nile/streams/app-configs/remus-test-enrichment) |
