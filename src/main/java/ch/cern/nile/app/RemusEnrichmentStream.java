package ch.cern.nile.app;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonObject;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;

import ch.cern.nile.common.json.JsonSerde;
import ch.cern.nile.common.streams.AbstractStream;
import ch.cern.nile.common.streams.offsets.InjectOffsetProcessorSupplier;

public final class RemusEnrichmentStream extends AbstractStream {

    @Override
    public void createTopology(final StreamsBuilder builder) {
        builder.stream(getSourceTopic(), Consumed.with(Serdes.String(), new JsonSerde())).filter(this::filterRecord)
                .processValues(new InjectOffsetProcessorSupplier(), InjectOffsetProcessorSupplier.getSTORE_NAME())
                .filter((key, value) -> validateTimestamp(value))
                .to(getSinkTopic());
    }

    private boolean filterRecord(final String ignored, final JsonObject value) {
        return value != null && value.get("timestamp") != null && value.get("value") != null;
    }

    private boolean validateTimestamp(final JsonObject value) {
        boolean valid = true;
        try {
            final long unixSeconds = Long.parseLong(value.get("timestamp").getAsString());
            final Date date = new Date(unixSeconds);
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", java.util.Locale.ENGLISH);
            sdf.format(date);
            setLastReadOffset(value.get("offset").getAsLong());
        } catch (NumberFormatException e) {
            logStreamsException(e);
            valid = false;
        }
        return valid;
    }
}
